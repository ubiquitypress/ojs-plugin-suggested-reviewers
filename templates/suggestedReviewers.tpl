{**
 * plugins/generic/suggestedReviewers/suggestedReviewers.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Edit SuggestedReviewers suggestedReviewers 
 *
 *}
{if $enableRecommendedReviewers OR $enableExcludedReviewers}
<h2>
	{translate key="manager.submissions.suggestedReviewers.title"}
</h2>
<p>{translate key="manager.submissions.suggestedReviewers.help"}
{*
	<button class="tooltipButton has-tooltip v-tooltip-open" data-original-title="null" aria-hidden="true" aria-describedby="license-licenseTerms-tooltip-en_US">
		<span aria-hidden="true" class="fa fa-question-circle"></span>
		<span class="-screenReader"></span>
	</button>
	<span id="license-licenseTerms-tooltip-en_US" class="-screenReader">
		Please note that all peer reviewers must be wholly independent of the submitted manuscript and the authoring team (e.g. not from the same institution). Reviewers will be asked to declare all Competing Interests should they be selected for review. Non-institutional email addresses will not be accepted/used. The editorial team maintain complete discretion over whether a reviewer is invited to complete peer review.
	</span>
*}
</p>
{/if}
{if $enableRecommendedReviewers}
{fbvFormSection title="manager.submissions.suggestedReviewers.recommended.label" for="recommendedReviewers" }
	{fbvElement type="textarea" height=$fbvStyles.height.SHORT multilingual=true name="recommendedReviewers" id="recommendedReviewers" value=$recommendedReviewers|replace:'<br />':'' readonly=$readOnly }
{/fbvFormSection}
{/if}
{if $enableExcludedReviewers}
{fbvFormSection title="manager.submissions.suggestedReviewers.excluded.label" for="excludedReviewers" }
	{fbvElement type="textarea" height=$fbvStyles.height.SHORT multilingual=true name="excludedReviewers" id="excludedReviewers" value=$excludedReviewers|replace:'<br />':'' readonly=$readOnly }
{/fbvFormSection}
{/if}