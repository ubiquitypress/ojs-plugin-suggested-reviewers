# OJS Suggested Reviewers Plugin

This plugin is:
- adding Two settings - one to turn each option on/off on the submission form, 
  (location of settings: settings>workflow>review>setup)
- adding ‘Recommended Reviewers’ and ‘Excluded Reviewers’ into submission form
- displaying values on ‘submission > workflow > review > add reviewer’ page

# Installation

- Compress the plugin in a `tar.gz` file and upload it through OJS plugin manager.
- Please make sure that the plugin folder name is `suggestedReviewers`

# What's new

- Ability to add the data {$excludedReviewers} and {$recommendedReviewers} to mail templates

## OJS compatibility

* OJS `3.2.1-*`+: please install version `0.7.*`+
* OJS `3.3.0-14`+: please install version `0.7.10`+
* OJS `3.4.0-*`+: please install version `0.9.*`+
