<?php

/**
 * @file plugins/generic/compmetingInterests/SuggestedReviewersPlugin.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SuggestedReviewersPlugin
 * @ingroup plugins_generic_SuggestedReviewersPlugin
 *
 * @brief SuggestedReviewers plugin class
 */

import('lib.pkp.classes.plugins.GenericPlugin');
use \PKP\components\forms\FieldOptions;
class SuggestedReviewersPlugin extends GenericPlugin {
    /**
     * @copydoc Plugin::register()
     */
    function register($category, $path, $mainContextId = null) {
        $success = parent::register($category, $path, $mainContextId);
        if ($success && $this->getEnabled($mainContextId)) {
            // Get the plugin DAO
            $this->import('SuggestedReviewersDAO');
            $suggestedReviewersDao = new SuggestedReviewersDAO();
            DAORegistry::registerDAO('SuggestedReviewersDAO', $suggestedReviewersDao);

            // Override OJS templates
            HookRegistry::register('TemplateResource::getFilename', array($this, '_overridePluginTemplates'));

            // Hooks for Submission form step 3
            HookRegistry::register('Schema::get::publication', array($this, 'addToPublicationSchema'));
            HookRegistry::register('Templates::Submission::SubmissionMetadataForm::AdditionalMetadata', array($this, 'metadataFieldEdit'));
            HookRegistry::register('submissionsubmitstep3form::readuservars', array($this, 'metadataReadUserVars'));
            HookRegistry::register('submissionsubmitstep3form::execute', array($this, 'metadataExecute'));

            // Hook for Submission Workflow Add Reviewer
            HookRegistry::register('advancedsearchreviewerform::display', array($this, 'loadTemplateData'));

            // Hooks for Workflow Settings Reviews Tab
            HookRegistry::register('Schema::get::context', array($this, 'addToContextSchema'));
            HookRegistry::register('Form::config::before', array($this, 'addToForm'));

            // Tracking sending emails
            HookRegistry::register('Mail::send', array($this, 'sendMail'));

        }
        return $success;
    }

    /****************/
    /**** Plugin ****/
    /****************/
    
    /**
    * @copydoc Plugin::isSitePlugin()
    */
    function isSitePlugin() {
        // This is a site-wide plugin.
        return true;
    }

    /**
     * @copydoc Plugin::getDisplayName()
     * Get the plugin name 
     */
    function getDisplayName() {
        return __('plugins.generic.suggestedReviewers.displayName');
    }

    /**
     * @copydoc Plugin::getDescription()
     * Get the description
     */
    function getDescription() {
        return __('plugins.generic.suggestedReviewers.description');
    }

    /**
     * @copydoc Plugin::getInstallSitePluginSettingsFile()
     * get the plugin settings
     */
    function getInstallSitePluginSettingsFile() {
        return $this->getPluginPath() . '/settings.xml';
    }
    

    /**********************************************/
    /**** Workflow > Settings > Review > Setup ****/
    /**********************************************/

    /**
    * Extend the Publication entity's schema with an suggestedReviewers property
    * @param $hookName string
    * @param $args array
    *
    */
    public function addToContextSchema($hookName, $args) {

        $schema = $args[0];

        $schema->properties->enableSuggestedRevieweres = new stdClass();
        $schema->properties->enableSuggestedRevieweres->type = 'array';
        $schema->properties->enableSuggestedRevieweres->items = new stdClass();
        $schema->properties->enableSuggestedRevieweres->items->type = 'string';
        $schema->properties->enableSuggestedRevieweres->items->validation = ['in:reviewersToIncluded,reviewersToExcluded'];
    }

    /**
    * Extend Setup form to add suggestedReviewers input field
    * @param $hookName string
    * @param $form object
    *
    */
    public function addtoForm($hookName, $form) {

        // Only modify Review Setup form
        if (!defined('FORM_REVIEW_SETUP') || $form->id !== FORM_REVIEW_SETUP) {
            return;
        }

        // get the context
        $request = PKPApplication::get()->getRequest();
        $context = $request->getContext();

        // Add the fields to the form
        $form->addField(new FieldOptions('enableSuggestedRevieweres', [
            'label' => __('manager.setup.reviewOptions.suggestedRevieweres.title'),
            'description' => __('manager.setup.reviewOptions.suggestedRevieweres.description'),
            'options' => [
                [
                    'value' => 'reviewersToIncluded',
                    'label' => __('manager.setup.reviewOptions.suggestedRevieweres.reviewersToIncluded.label')
                ],
                [
                    'value' => 'reviewersToExcluded', 
                    'label' => __('manager.setup.reviewOptions.suggestedRevieweres.reviewersToExcluded.label')
                ],
            ],
            'value' => $context->getData('enableSuggestedRevieweres') ? $context->getData('enableSuggestedRevieweres') : [],
            ]),array(FIELD_POSITION_BEFORE,'defaultReviewMode')
        );
    }


    /****************************************/
    /***** Submissions > Enter Metadata *****/
    /****************************************/

    /**
     * Extend the publication entity's schema with recommendedReviewers and excludedReviewers properties
     * @param $hookName string
     * @param $args array
     *
     */
    public function addToPublicationSchema($hookName, $args)
    {
        $schema = $args[0];
        $schema->properties->recommendedReviewers = new stdClass();
        $schema->properties->recommendedReviewers->type = 'string';
        $schema->properties->recommendedReviewers->multilingual = true;
        $schema->properties->recommendedReviewers->validation = ['nullable'];

        $schema->properties->excludedReviewers = new stdClass();
        $schema->properties->excludedReviewers->type = 'string';
        $schema->properties->excludedReviewers->multilingual = true;
        $schema->properties->excludedReviewers->validation = ['nullable'];
    }

    /**
     * Insert the fields template into the form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataFieldEdit($hookName, $params) {
        // get the template info
        $smarty =& $params[1];
        $output =& $params[2];

		$request = PKPApplication::get()->getRequest();
		$templateMgr = TemplateManager::getManager($request);

		// get the current fields current values from DB
		$journalId = $request->getContext()->getId();

		$submissionId = $_GET['submissionId'] ?: $_POST['submissionId'];  // Retrieving the submissionId from the URL
		$submissionDao = DAORegistry::getDAO('SubmissionDAO');
		$submission = $submissionDao->getById($submissionId);

		$journalDao = DAORegistry::getDAO('JournalDAO');
		$journal = $journalDao->getById($journalId);
		$enableSuggestedRevieweres = ($journal) ? $journal->getData('enableSuggestedRevieweres') : [];

		$enableRecommendedReviewers = ($enableSuggestedRevieweres && in_array('reviewersToIncluded', $enableSuggestedRevieweres)) ? true : false;
		$enableExcludedReviewers = ($enableSuggestedRevieweres && in_array('reviewersToExcluded', $enableSuggestedRevieweres)) ? true : false;

        if ($submission) {
    		// get the publiction id and locale
    		$publication = $submission->getCurrentPublication();

    		$locale = $publication->getData('locale');

    		// get the values
    		$recommendedReviewers = $publication->getData('recommendedReviewers');
    		$excludedReviewers = $publication->getData('excludedReviewers');

    		if (!$recommendedReviewers) {
    			$recommendedReviewers = array($locale => null);
    		}
    		if (!$excludedReviewers) {
    			$excludedReviewers = array($locale => null);
    		}

    		$templateVars = array('excludedReviewers' => $excludedReviewers,
    		 	 'recommendedReviewers' => $recommendedReviewers,
    	  		'enableExcludedReviewers' => $enableExcludedReviewers,
    		  	'enableRecommendedReviewers' => $enableRecommendedReviewers);

    		$templateMgr->assign($templateVars);

    		$template = file_get_contents($this->getTemplatePath() . '/suggestedReviewers.tpl');
    		$output .= $smarty->fetch('string:' . $template);
        }
    }

    /**
     * Get users input values from the form
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataReadUserVars($hookName, $params) {
        // get the template
        $request = PKPApplication::get()->getRequest();
        $templateMgr = TemplateManager::getManager($request);
        // get the form values
        $form = $params[0];
        // add the new field values to the user variables
        $userVars =& $params[1];
        $userVars[] = 'recommendedReviewers';
        $userVars[] = 'excludedReviewers';

        // get the current fields current values from DB
        $journalId = $form->context->getData('id');
        $journalDao = DAORegistry::getDAO('JournalDAO');
        $journal = $journalDao->getById($journalId);
        $enableSuggestedRevieweres = $journal->getData('enableSuggestedRevieweres');
        $enableSuggestedRevieweres = $enableSuggestedRevieweres ?: [];
        $enableRecommendedReviewers = (in_array('reviewersToIncluded', $enableSuggestedRevieweres)) ? true : false;
        $enableExcludedReviewers = (in_array('reviewersToExcluded', $enableSuggestedRevieweres)) ? true : false;

        // add the current data to the template
        $templateVars = array('enableExcludedReviewers' => $enableExcludedReviewers,
                              'enableRecommendedReviewers' => $enableRecommendedReviewers);

        $templateMgr->assign($templateVars);
    }

    /**
     * Set suggestedReviewers value
     *
     * @param $hookName string
     * @param $params array
     *
     */
    function metadataExecute($hookName, $params) {
        // submitting the form with the new values
        // get the current journalId
        $form =& $params[0];

        // get the fields options
        $journalId = $form->context->getData('id');
        $journalDao = DAORegistry::getDAO('JournalDAO');
        $journal = $journalDao->getById($journalId);
        $enableSuggestedRevieweres = $journal->getData('enableSuggestedRevieweres');
        $enableSuggestedRevieweres = ($enableSuggestedRevieweres) ? $enableSuggestedRevieweres : [];
        $enableRecommendedReviewers = (in_array('reviewersToIncluded', $enableSuggestedRevieweres))?true:false;
        $enableExcludedReviewers = (in_array('reviewersToExcluded', $enableSuggestedRevieweres))?true:false;

        $publication = $form->submission->getCurrentPublication();
        
        $pubId = $publication->getData('id');
        $locale  = $publication->getData('locale');

        // get the form current values
        $recommendedReviewers = $form->getData('recommendedReviewers');
        $excludedReviewers = $form->getData('excludedReviewers');

        // update the data in DB
        $suggestedReviewersDao = DAORegistry::getDAO('SuggestedReviewersDAO');
        
        if ($enableRecommendedReviewers) {    
            foreach ($recommendedReviewers as $locale => $value) {
                $suggestedReviewersDao->replaceSuggestedReviewers($pubId, $locale, 'recommendedReviewers',$value);
            }
        }
        if ($enableExcludedReviewers) {
            foreach ($excludedReviewers as $locale => $value) {
                $suggestedReviewersDao->replaceSuggestedReviewers($pubId, $locale, 'excludedReviewers',$value);
            }
        }
    }

    /**
     * Add data to email templates.
     *
     * @param string $hookname
     * @param array $args
     *
     */
    public function sendMail($hookName, $args) {
        $form = &$args[0];
		if (property_exists($form, 'submission')) {
			$submission = $form->submission;
			$publication  = $submission->getCurrentPublication();
			$excludedReviewers = $publication->getLocalizedData('excludedReviewers');
			$recommendedReviewers = $publication->getLocalizedData('recommendedReviewers');
			$args[0]->privateParams['{$excludedReviewers}'] = htmlspecialchars($excludedReviewers);
			$args[0]->privateParams['{$recommendedReviewers}'] = htmlspecialchars($recommendedReviewers);
		}
    }

    /**
     * Fired when add reviewer pop-up is called in Submissions > Workflow > Review.
     *
     * @param string $hookname
     * @param array $args
     *
     */
    public function loadTemplateData($hookName, $args) {
        // get the form
        $request = PKPApplication::get()->getRequest();
        $form =& $args[0];
        
        // get suggestedReviewers values    
        $publication = $form->getSubmission()->getCurrentPublication();
        $recommendedReviewers = $publication->getLocalizedData('recommendedReviewers');
        $excludedReviewers = $publication->getLocalizedData('excludedReviewers');

        if (!$recommendedReviewers) {
            $recommendedReviewers = null;
        }
        if (!$excludedReviewers) {
            $excludedReviewers = null;
        }

		//get authors and affiliations
		$authors = [];
		foreach($publication->getData('authors') as $author) {
			$affiliations = [];
			foreach($author->getAffiliation(null) as $affiliationName) {
				$affiliations[] = $affiliationName;
			}

			$authors[$author->getFullName()] = implode(',', array_filter($affiliations));
		}

        // add the current data to the template
        $templateVars = array('excludedReviewers' => $excludedReviewers,
                              'recommendedReviewers' => $recommendedReviewers,
                              'authors' => $authors);

        $templateMgr = TemplateManager::getManager($request);
        $templateMgr->assign($templateVars);
    }

}

